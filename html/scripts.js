window.addEventListener('DOMContentLoaded', ()=> {
	if (localStorage.getItem('theme') === 'dark') {
		swapTheme();
	}
})

function swapTheme() {
	var element = document.body;
	element.classList.toggle("darkmode");

	if (document.getElementById('click').getAttribute('src') == '/img/dark.svg') {
		document.getElementById('click').setAttribute('src','/img/light.svg');
		localStorage.setItem('theme', 'dark');
		document.getElementById('flag').setAttribute('title', 'trans rights are human rights');
	} else {
		document.getElementById('click').setAttribute('src','/img/dark.svg');
		localStorage.setItem('theme', 'light');
		document.getElementById('flag').setAttribute('title', 'protect queer kids');
	}
}
